//
//  ShowPostViewController.swift
//  MyTwitterApp
//
//  Created by Safia CHMITI on 03/08/2018.
//  Copyright © 2018 Safia CHMITI. All rights reserved.
//

import UIKit
import Firebase

class ShowPostViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    @IBOutlet var sfeirientableview: UITableView!
    @IBOutlet var searchBar: UISearchBar!
    var sfeirtable = [Model]()
    var sfeirtablefilter = [Model]()
    
    var refsferiens: DatabaseReference!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchbar()
       
        refsferiens = Database.database().reference().child("sfeirians")
        refsferiens.observe(DataEventType.value) { (snapshot) in
            if snapshot.childrenCount>0 {
                self.sfeirtable.removeAll()
                
                for sferiens in snapshot.children.allObjects as! [DataSnapshot] {
                    let sferienobject = sferiens.value as? [String: AnyObject]
                    let sferienname = sferienobject?["nom"]
                    let sferienprenom = sferienobject?["prénom"]
                    let sferientec = sferienobject?["taille_de_t_shirt"]
                
                    let sferien = Model(name: (sferienname as! String?)!, prenom: sferienprenom as! String, tec: sferientec as! String)
                    self.sfeirtable.append(sferien)
                    self.sfeirtablefilter = self.sfeirtable
                }
                self.sfeirientableview.reloadData()
                print("numbers of sfeiriens")
                print(self.sfeirtable.count)
            }
        }
        
    }
    
    func searchbar(){
        searchBar.delegate = self
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sfeirtablefilter.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! CellTableViewCell
        let sferien: Model
        sferien = sfeirtablefilter[indexPath.row]
        cell.prenomtextlabel.text = sferien.prenom
        cell.nametextlabel.text = sferien.name
       // cell.tectextlabel.text = sferien.tec
        return cell
        
        
    }
    // search bar
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        guard !searchText.isEmpty else {
            sfeirtablefilter = sfeirtable
            sfeirientableview.reloadData()
            return
        }
        
        sfeirtablefilter = sfeirtable.filter({ (person) -> Bool in
            (person.name?.lowercased().contains(searchText.lowercased()))!
            })
        sfeirtablefilter = sfeirtable.filter({ (person) -> Bool in
            (person.tec?.lowercased().contains(searchText.lowercased()))!
        })
        sfeirientableview.reloadData()
        }

    
}
