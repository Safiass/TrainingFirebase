//  ViewController.swift
//  MyTwitterApp
import UIKit
import Firebase
import FirebaseAuth
import FirebaseStorage
import FirebasePerformance
import FirebaseDatabase

class ViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    //connection des buttons
    @IBOutlet var txtFullName: UITextField!
    @IBOutlet var txtEmail: UITextField!
    @IBOutlet var txtPwd: UITextField!
    @IBOutlet var userImage: UIImageView!
   
    
    var imagepicker: UIImagePickerController! // declaration pour utiliser la caméra et fait faire appel aussi à                                                         UIImagePickerControllerDelegate et UINavigationControllerDelegate
   
    var ref = DatabaseReference.init() //instance a reference for the database
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        let ref = Database.database().reference(withPath: "sfeirians")
        //let ref = Database.database().reference().child("chmiti_m")
        ref.observeSingleEvent(of: .value, with: { snapshot in
           
            if !snapshot.exists() {
                print("does not exist")
               
            }
            //print("datadata")
            //print(snapshot) // Its print all values including Snap (User)
           // print("datadata1")
           // print(snapshot.value!)
           // print("datadata2")
            
           // let username = snapshot.childSnapshot(forPath: "full_name").value
           // print(username!)
            
        })
        
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        //initialize imagepicker
        imagepicker = UIImagePickerController()// Initialize the image picker
        imagepicker.delegate = self
        self.ref = Database.database().reference()//Initiate ref
       }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    // if someone wants to select image
    @IBAction func addImage(_ sender: Any) {
        present(imagepicker, animated: true, completion: nil) // we have image and know we add imagePickerController
        
    }
 
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let imageuser = info[UIImagePickerControllerOriginalImage] as? UIImage {
            userImage.image = imageuser
            }
           
        imagepicker.dismiss(animated: true, completion: nil)
    }
    //S'authentifier
   
    @IBAction func actionLogin(_ sender: Any) {
        Auth.auth().signIn(withEmail: txtEmail.text!, password: txtPwd.text!){
            (user,error) in
            if let error = error {
                print("error login : \(error)")
                } else {
                print("successful sign in")
                self.postonsegue()
               }
        }
       
      }
    // S'inscrire
    @IBAction func actionRegister(_ sender: Any) {
        Auth.auth().createUser(withEmail: txtEmail.text!, password: txtPwd.text!){
            (user,error) in
            if let error = error {
                print("error register in : \(error)")
            } else {
                print("created user")
                //Upload Image to Firebase
                let storage = Storage.storage().reference(forURL: "gs://mytwitterapp-75f00.appspot.com") //define the url that we want to upload image to
                var data = NSData() // type de donnée
                let imageuser = self.userImage.image
                data = UIImageJPEGRepresentation(imageuser!, 0.8)! as NSData
                let dataformat = DateFormatter()
                dataformat.dateFormat = "MM_DD_yy_h_mm_a"
                let imageName = "\(Auth.auth().currentUser!.uid)_ \(dataformat.string(from: NSDate() as Date))"
                let imagepath = "UsersImages/ \(imageName).jpg"
                let childUserImages = storage.child(imagepath)
                let metaData = StorageMetadata()
                metaData.contentType = "image.jpeg"
                childUserImages.putData(data as Data, metadata: metaData)
                self.saveInfoToDatabase(UserImagePath: imagepath, UserName: self.txtFullName.text!)
                self.postonsegue()
            }
        }
        
        
    }
    
    func saveInfoToDatabase(UserImagePath:String, UserName:String){
        let msg = ["fullName":UserName, "UserImagePath": UserImagePath]
        self.ref.child("Users").child((Auth.auth().currentUser?.uid)!).setValue(msg)
        }
    
    func postonsegue(){
        performSegue(withIdentifier: "showpost", sender: Auth.auth().currentUser?.uid)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showpost" {
            if segue.destination is ShowPostViewController{
                print("hehe")
                
            }
        } else if segue.identifier == "searchsomeone"{
            if segue.destination is SearchViewController {
                print("haha")
            }
            
        }
    }
    
    func searchUi(){
        performSegue(withIdentifier: "searchsomeone", sender: AnyObject.self)
    }
    
    
    @IBAction func searchSomeOne(_ sender: Any) {
        self.searchUi()
        
    }
    
}

