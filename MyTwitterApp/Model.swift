//
//  Model.swift
//  MyTwitterApp
//
//  Created by Safia CHMITI on 06/08/2018.
//  Copyright © 2018 Safia CHMITI. All rights reserved.
//

import Foundation

class Model{
    var name: String?
    var prenom: String?
    var tec: String?
    
    init(name: String, prenom: String, tec: String){
        self.name = name
        self.prenom = prenom
        self.tec = tec
    }
}
